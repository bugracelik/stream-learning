package com.bugra.testerapp;

import com.bugra.testerapp.helper.YıldızBas;
import net.bytebuddy.utility.RandomString;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

//@SpringBootTest
class DemoApplicationTests extends Object {

    private static Connection connection;

    @BeforeAll
    static void name1() throws SQLException {

        System.out.println("connection alınıyor");
        String digitalOceanMySqlServerHost = "mysql-digital-ocean-cluster-do-user-8063408-0.b.db.ondigitalocean.com";
        String port = "25060";
        String schemaName = "/trendyoldb";
        String url = "jdbc:mysql://" + digitalOceanMySqlServerHost + ":" + port + schemaName;

        connection = DriverManager.getConnection(url, "doadmin", "tcuj0v9mconi1cjv");
    }

    @Test
    void contextLoads() {
        System.out.println("hello");
    }

    @Test
    void object() {
        Object asd = new DemoApplicationTests();

        String x = (String) asd;

        Object o = "1";
        Object adas = 1;


    }

    @Test
    void get_element_from_object_list() {
        List asList = Arrays.asList("123", 23, 123, 312); // <>
        System.out.println(asList.get(0));
        System.out.println(asList.get(1));
        System.out.println(asList.get(2));
        System.out.println(asList.get(3));

        Object o = asList.get(1);
        System.out.println(o.toString());
    }

    @Test
    void deneme1() {
        List<Integer> numberList = Arrays.asList(8, 3, 1, 5, 17, 21, 10, 2);
        long count = numberList.stream().count();
        Stream<Integer> integerStream = numberList.stream().filter(val -> val > 5);

        numberList.stream().forEach(val -> System.out.println(val));
        YıldızBas.bas(10);
        integerStream.forEach(System.out::println);
        YıldızBas.bas(5);

        numberList.
                stream().
                filter(val -> val % 2 == 0).
                forEach(val -> System.out.println(val));
    }

    @Test
    void deneme2() {
        List numberList = Arrays.asList(8, 3, 1, 5, 17, 21, 10, 2, 10, 2, 2, 3, 4, 3);
        numberList.stream().distinct().forEach(System.out::println);
    }

    @Test
    void deneme3() {
        List<Integer> numberList = Arrays.asList(8, 3, 1, 5, 17, 21, 10, 2, 10, 2, 2, 3, 4, 3);
        List<Integer> list = new ArrayList<Integer>(5);
        numberList.parallelStream().forEach(val -> System.out.println(val));
        YıldızBas.bas(10);
        numberList.stream().forEach(System.out::println);
        list.add(0, 0);
        list.add(1, 1);
        list.add(2, 2);
        list.add(3, 3);
        list.add(4, 4);
        list.add(4, 5);
        list.add(4, 6);

        System.out.println(list);
        YıldızBas.bas(10);
        list.stream().forEach(System.out::println);
    }

    @Test
    void deneme4() {
        List names = Arrays.asList("Ali", "Veli", "Selami", "Cem", "Zeynel", "Can", "Hüseyin");

        Stream stream = names.stream();

        stream.forEach(name -> System.out.println(name));

        stream.forEach(System.out::println);
    }

    @Test
    void deneme5() {
        List names = Arrays.asList("Ali", "Veli", "Selami", "Cem", "Zeynel", "Can", "Hüseyin");
        List<String> stringList = new ArrayList<String>();
        stringList = new ArrayList<String>();
        stringList.add("ali");
        stringList.add("merhmet");
        stringList.add("asdas");
        stringList.add("dasdasd");
        for (String str : stringList)
            System.out.println(str);
        //neden string yazamadık?
        for (Object name : names)
            System.out.println(name);
    }

    @Test
    void deneme6() {
        List<String> myList = Arrays.asList("merhaba", "dünya", "ben", "yazılımci", "oluyorum");
        myList.stream()
                .filter(val -> val.length() <= 5)
                .forEach(System.out::println);
    }

    @Test
    void test1() {
        IntStream stream = IntStream.of(13, 1, 3, 5, 8, 1, 13, 2, 8);
        stream.sorted().distinct().forEach(value -> System.out.println(value));
    }

    @Test
    void test2() {
        List<Integer> numberList = Arrays.asList(8, 3, 1, 5, 17, 21, 10, 2, 12, 213, 123, 123, 321, 123);
        numberList
                .stream()
                .limit(10)
                .forEach(System.out::println);
    }

    @Test
    void test3() {
        List names = Arrays.asList("ali", "veli", "selami", "can", "hüseyin");
        Object collect = names.stream().collect(Collectors.toList());
        Object collect1 = names.stream().collect(Collectors.toSet());
        Long collect2 = (Long) names.stream().collect(Collectors.counting());
        String collect3 = (String) names.stream().collect(Collectors.joining(" - "));
        System.out.println(collect3);
    }

    @Test
    void test4() {
        List <String> names  = Arrays.asList("ali", "veli", "seLAmi", "CeM", "BuGra");
        //names.stream().filter(name -> name.toUpperCase());
        names.stream().map(name -> name.toUpperCase()).forEach(System.out::println);
        names.stream().map(name -> name.toLowerCase()).forEach(System.out::println);
    }

    @Test
    void test5(){
        List <Integer> myNumber = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9);
        myNumber
                .stream()
                .map(value -> value * value)
                .forEach(System.out::println);
    }

    @Test
    void test6() {
        List<String> list = Arrays.asList("merhaba", "ben", "buğra", "celik");
        Stream<String> stringStream = list.stream().filter(val -> val.length() == 5);
        stringStream.forEach(System.out::println);
        YıldızBas.bas(2);
        Stream<String> resultList = list.stream().filter(val -> val.contains("e"));
        resultList.forEach(System.out::println);
        YıldızBas.bas(3);
        list.stream().filter(val -> val.endsWith("n")).forEach(System.out::println);
        YıldızBas.bas(4);
        list.stream().filter(val -> val.startsWith("mer")).filter(val -> val.length() == 7).forEach(System.out::println);
    }

    @Test
    void filter_büyük_length_1_(){
        List<String> myList = Arrays.asList("merhaba", "ben", "çelik", "naber");
        Stream<String> stringStream = myList.stream().filter(val -> val.length() >= 1);

        Stream<String> list = stringStream.filter(val -> val.contains("e"));

        Stream<String> stringStream1 = list.map(val -> val.toUpperCase());

        stringStream1.forEach(System.out::println);


        myList.stream().map(val -> val.toUpperCase()).forEach(System.out::println);
    }

    @Test
    void db_list_stream() throws SQLException {

        ResultSet resultSet = connection.prepareStatement("select * from categories").executeQuery();


        //ResultSet resultSet = connection.prepareStatement("insert into categories(CategoryName, Description) values ('%s', '%s')").executeQuery();
        List<Category> categortyList = new ArrayList<>();

        while (resultSet.next()) {
            int categoryId = resultSet.getInt("CategoryId");
            String categoryName = resultSet.getString("CategoryName");
            String description = resultSet.getString("Description");

            Category category = new Category(categoryId, categoryName, description);
            categortyList.add(category); // resultset-to-list
        }

        long before = LocalDateTime.now().getSecond();

        List<CategoryDto> dtoList = categortyList
                                                    .stream()
                                                    .map(categoryFromDatabaseOneInstance -> new CategoryDto(categoryFromDatabaseOneInstance.getName()))
                                                    .collect(Collectors.toList());


        long after = LocalDateTime.now().getSecond();
        System.out.println(after - before);
        // System.out.println(dtoList);
    }


    @Test
    void insert() throws SQLException {

        while (true) {
            String sqlFmt = "INSERT INTO categories (CategoryName, Description) VALUES ('%s', '%s');";
            String uniqCategoryName = RandomString.make();
            String description = RandomString.make();
            String formattedSql = String.format(sqlFmt, uniqCategoryName, description);
            connection.prepareStatement(formattedSql).execute();
        }

    }

    @Test
    void random_String() {
        String categoryName = RandomString.make();
        System.out.println(categoryName);
    }

    @Test
    void hashmap() {
        HashMap<String, String> map = new HashMap();

        map.put("istanbul", "34"); // hash -> a[12312] = 34;
        map.put("ankara", "06");

        String plaka = map.get("istanbul");
        String izmir = map.get("izmir");

        System.out.println("ankara" + map.get("ankara"));
        System.out.println(izmir);
        System.out.println(plaka);


        List<Integer> numbers = Arrays.asList(4, 8, 3, 1);

        System.out.println(numbers.get(0));

        // 0 1  2  3
        int a[] = {4, 8, 3, 1}; // 0 - 4
        System.out.println(a[1]);
    }


    @Test
    void linear_search_to_hash_search_why_tek_atış_hız_1000ms_1saniye_cb_10ms_5ms_150m_document() {

        int a[] = {4, 8, 3, 17, 18, 9, 21, 20, 5, 1};

        // linear search -> O( n ) n tane if işlemi
        for (int i = 0; i < a.length; i++) {
            if (a[i] == 1)
                System.out.println("var"); // 10000000 ms
        }

        // ama dizi sıralı binary search sıralama + O( log(n) ) 1 2 3 ....64.....90.  110 120 124 126 127 128              1 2 3 4 5 6 7 en kötü
        int anınsiralihali [] = {1, 4, 8, 9, 20, 21, 25};


        // hash search
        int[] a1 = new int[1000];
        System.out.println(a1[0]);

        a1[1] = 1;
        a1[2] = 2;

        a1[999] = 999;

        if (a1[999] != 0) { // 0( 1 ) - 1000ms
            System.out.println("999 var");
        }





    }
}

class CategoryDto {
    private String name;

    public CategoryDto(String name) {
        this.name = name;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "CategoryDto{" +
                "name='" + name + '\'' +
                '}';
    }
}

class Category { // domain objesi dönülmez dto dönülür response dönülür.
    private  int id;
    private  String name;
    private  String description;

    public Category(int categoryId, String categoryName, String description) {
        this.id = categoryId;
        this.name = categoryName;
        this.description = description;
    }

    @Override
    public String toString() {
        return "Category{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
